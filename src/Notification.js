var Notification = cc.LayerColor.extend({
    init: function(n) {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.setBg(n);
    },
    setBg: function(n){
        if(n==1){
            this.bg = new cc.Sprite.create("res/images/WinOrLose/win.png");
            this.bg.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
            this.addChild(this.bgfarm);
        }else{
            this.bg = new cc.Sprite.create("res/images/WinOrLose/lose.png");
            this.bg.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
            this.addChild(this.bgfarm);
        }
        
    }
});
