var Egg = cc.Sprite.extend({
    ctor: function(x,y,n) {
        this._super();
        this.number = n;
        this.initWithFile( 'res/images/Egg/egg1.png' );
        this.movingAction = this.createAnimationAction();
        this.runAction( this.movingAction );
        if(startScene.direction == StartScene.DIR.FARM){
        	this.addEgg(x,y);
        }
        this.autoRemoveEgg();
        this.pos = 0;

    },
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/Egg/egg1.png' );
        animation.addSpriteFrameWithFile( 'res/images/Egg/egg2.png' );
        console.log( animation.getDelayPerUnit() );
        animation.setDelayPerUnit( 0.7 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    autoRemoveEgg: function(){
        this.scheduleOnce(function(){
        startScene.currentScene.chickenCor.removeChild(this)
        },10);
        this.scheduleOnce(function(){
        eggCheck[this.number] = null;
        },10);
        
    },
    addEgg: function(x,y){
    	this.setPosition( new cc.Point(x,y) );
    	startScene.currentScene.chickenCor.addChild(this);
        this.pos = this.getPosition();
        console.log("egg"+(this.number+1)+" added location x = "+this.pos.x+", y = "+this.pos.y);
        eggCheck[this.number] = this;
    },
});
