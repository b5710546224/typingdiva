var res = {
	farmBG_png : "res/images/farmBG.png",
	playerRight_png : "res/images/Player/playerRight.png",
	playerLeft_png : "res/images/Player/playerLeft.png",
	chickenCorral_png : "res/images/ChickenCorral/chickenCorral.png",
	cowCorral_png : "res/images/CowCorral/cowCorral.png",
	leftArrow1_png : "res/images/Arrow/leftArrow1.png",
	leftArrow2_png : "res/images/Arrow/leftArrow2.png",
	rightArrow1_png : "res/images/Arrow/rightArrow1.png",
	rightArrow2_png : "res/images/Arrow/rightArrow2.png",
	factoryBG_png : "res/images/factoryBG.png",
	eggMachine_png : "res/images/EggMachine/eggMachine.png",
	tick_png : "res/images/tick.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
