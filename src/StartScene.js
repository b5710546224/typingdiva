var tickBox = [];
var levelGoal = [];
var curentScore = 5000;
var money = 0;
var playing = true;
var goal = 5000;
var eggNum = 0;
var milkNum = 0;
var currentEggNum = 0;
var currentMilkNum = 0;
var cowNum = 0;
var chickNum = 0;
var pancakeNum = 0;
var openStore = false;
var min = 5;
var sec = 0;
var eggGoal = 20;
var milkGoal = 1;
var cowGoal = 1;
var chickenGoal = 4;
var currentLevel = -1;
var count = 0;
var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        this.queqe = 0;
        this.level = [];
        this.setLevelData();
        this.setLevel(5);
        this.farmScene = new FarmLayer();
        this.levelScene = new LevelPage();
        this.startPage = new StartLayer();
        this.currentScene = this.farmScene;
        this.currentScene.init();
        this.addChild( this.currentScene );
        this.direction = StartScene.DIR.FARM;
        this.schedule(this.checkGame,3);
        this.addScoreBoard();
        this.addMouseHandlers();
        this.addScoreLabel();
        this.addGoalLabel();
        this.addCurrentProLabel();
        this.addStoreBut();
        this.addClockBase();
        this.schedule(this.updateScore,0.01);
        this.schedule(this.countdown,1);
        this.schedule(this.updateCurrentPro,0.01);

    },
    setLevelData: function(){
        this.level[0] = [250,3,0,1000,10,0,0,2];
        this.level[1] = [500,2,30,2000,25,0,0,2];
        this.level[2] = [250,5,0,5000,20,5,1,4];
        this.level[3] = [400,4,30,6500,40,2,1,4];
        this.level[4] = [2000,5,0,10000,45,10,2,4];
        this.level[5] = [5000,7,30,35000,70,30,4,4];
        this.level[6] = [250,15,0,45000,100,50,4,4];

    },
    checkGame: function(){
        if(min==0&&sec==0&&currentEggNum>=eggGoal&&currentMilkNum>=milkGoal&&curentScore>=goal&&cowNum>=cowGoal&&chickNum>=chickenGoal){
            count++;
            playing=false;
            markBoxCheck[currentLevel] = true;
            this.noti = new cc.Sprite.create("res/images/WinOrLose/win.png");
            this.noti.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
            this.addChild(this.noti);
            this.addButtonNoti();
        }
        else{
            if(min==0&&sec==0){
                playing=false;
                this.noti = new cc.Sprite.create("res/images/WinOrLose/lose.png");
                this.noti.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
                this.addChild(this.noti);
            }
        }
    },
    addButtonNoti: function(){
            this.goback = new cc.Sprite.create("res/images/WinOrLose/gobackBut1.png");
            this.goback.setPosition(new cc.Point(screenWidth/2,150));
            this.addChild(this.goback);
    },
    setLevel: function(index){
        currentLevel = index;
        curentScore = this.level[index].shift();
        min = this.level[index].shift();
        sec = this.level[index].shift();
        goal = this.level[index].shift();
        eggGoal = this.level[index].shift();
        milkGoal = this.level[index].shift();
        cowGoal = this.level[index].shift();
        chickenGoal = this.level[index].shift();
    },
    addClockBase: function(){
        this.bClock = new cc.Sprite.create("res/images/Clock/bgClock.png");
        this.bClock.setPosition( new cc.Point( 105, 543 ) );
        this.addChild(this.bClock);
        this.labelMin = cc.LabelTTF.create( min, 'Arial', 60 );
        this.labelMin.setPosition( new cc.Point( 60, 543 ) );
        this.addChild(this.labelMin);

        this.space = cc.LabelTTF.create( ':', 'Arial', 60 );
        this.space.setPosition( new cc.Point( 100, 545 ) );
        this.addChild(this.space);

        this.labelSec = cc.LabelTTF.create( '0', 'Arial', 60 );
        this.labelSec.setPosition( new cc.Point( 140, 543 ) );
        this.addChild(this.labelSec);
    },
    addMouseHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
            onMouseDown: function(event){
                var loc = event.getLocation();
            
                var getLoc = event.getLocation();
                console.log(chickQueue);
                console.log(chickCheck);
                console.log(chickFoodCheck);
                console.log(queue);

                if(event.getLocation().y<=334.3035343035343&&!openStore&&playing&&self.direction==StartScene.DIR.FARM){
                    console.log("tickBox");
                    tickBox.push(new Tick());
                    tickBox[tickBox.length-1].setPosition(new cc.Point(getLoc.x,getLoc.y));
                    startScene.addChild(tickBox[tickBox.length-1]);
                }else{
                    if(!playing&&self.direction==StartScene.DIR.FARM){
                    console.log("notplaing x = "+event.getLocation().x+", y = "+event.getLocation().y);
                    if(loc.x>=337&&loc.x<=463&&loc.y<=171&&loc.y>=131){
                        self.goback.setTexture("res/images/WinOrLose/gobackBut3.png");
                        self.switchToLevel();
                    }
                }
                    if(self.direction==StartScene.DIR.FARM){
                        if(loc.y>=437&&loc.y<=477&&loc.x>=639&&loc.x<=771&&!openStore&&playing){
                            self.storeBut.setTexture('res/images/Board/storeBut3.png');
                            self.openStore();
                            openStore = true;

                        }else if(openStore){
                            if(loc.x>=640&&loc.x<=675&&loc.y>=477&&loc.y<=510&&(!eggSelling&&!milkSelling)&&playing){
                                self.removeChild(self.store);
                                openStore = false;
                            }
                        }
                    }      
                }

            },
            onMouseMove: function(event){
                var loc = event.getLocation();
        
                if(self.direction==StartScene.DIR.FARM){
                    if(loc.y>=437&&loc.y<=477&&loc.x>=639&&loc.x<=771&&!openStore&&playing){self.storeBut.setTexture('res/images/Board/storeBut1.png');}
                    else{self.storeBut.setTexture('res/images/Board/storeBut2.png');}
                }
            }
        }, this);
    },
    countdown: function(){
        if(sec>0){
            sec-=1;
        }else if(sec==0&&min!=0){
            min-=1;
            sec=59;
        }
        else if(sec==0&&min==0){

        }
        this.updateTime(min,sec);
    },
    updateTime: function(min,sec){
        this.labelMin.setString(min);
        this.labelSec.setString(sec);
    },
    addScoreLabel: function(){
        this.scoreLabel = cc.LabelTTF.create( '0', 'Arial', 30 );
        this.scoreLabel.setPosition( new cc.Point( 700, 520 ) );
        this.addChild(this.scoreLabel);
    },
    addCurrentProLabel: function(){
        this.eggProLabel = cc.LabelTTF.create( ""+currentEggNum, 'Arial', 25 );
        this.eggProLabel.setPosition( new cc.Point( 275, 525 ) );
        this.addChild(this.eggProLabel);

        this.milkProLabel = cc.LabelTTF.create( ""+currentMilkNum, 'Arial', 25 );
        this.milkProLabel.setPosition( new cc.Point( 305, 465 ) );
        this.addChild(this.milkProLabel);

        this.cowProLabel = cc.LabelTTF.create( ""+cowNum, 'Arial', 25 );
        this.cowProLabel.setPosition( new cc.Point( 445, 525 ) );
        this.addChild(this.cowProLabel);

        this.chickProLabel = cc.LabelTTF.create( ""+chickNum, 'Arial', 25 );
        this.chickProLabel.setPosition( new cc.Point( 440, 465 ) );
        this.addChild(this.chickProLabel);
    },
    updateCurrentPro: function(dt){
        this.eggProLabel.setString(currentEggNum);
        this.milkProLabel.setString(currentMilkNum);
        this.cowProLabel.setString(cowNum);
        this.chickProLabel.setString(chickNum);
    },
    addGoalLabel: function(){
        this.goalLabel = cc.LabelTTF.create( goal, 'Arial', 25 );
        this.goalLabel.setPosition( new cc.Point( 735, 560 ) );
        this.addChild(this.goalLabel);

        this.eggGoalLabel = cc.LabelTTF.create( "/"+eggGoal, 'Arial', 25 );
        this.eggGoalLabel.setPosition( new cc.Point( 310, 525 ) );
        this.addChild(this.eggGoalLabel);

        this.milkGoalLabel = cc.LabelTTF.create( "/"+milkGoal, 'Arial', 25 );
        this.milkGoalLabel.setPosition( new cc.Point( 330, 465 ) );
        this.addChild(this.milkGoalLabel);

        this.cowGoalLabel = cc.LabelTTF.create( "/"+cowGoal, 'Arial', 25 );
        this.cowGoalLabel.setPosition( new cc.Point( 470, 525 ) );
        this.addChild(this.cowGoalLabel);

        this.chickGoalLabel = cc.LabelTTF.create( "/"+chickenGoal, 'Arial', 25 );
        this.chickGoalLabel.setPosition( new cc.Point( 465, 465 ) );
        this.addChild(this.chickGoalLabel);
    },
    removeAllLabel: function(){
        self.removeChild(self.scoreLabel);
        self.removeChild(self.scoreBoard);
        self.removeChild(self.storeBut);
        self.removeChild(self.goalLabel);
        self.removeChild(self.eggGoalLabel);
        self.removeChild(self.milkGoalLabel);
        self.removeChild(self.cowGoalLabel);
        self.removeChild(self.chickGoalLabel);
        self.removeChild(self.eggProLabel);
        self.removeChild(self.milkProLabel);
        self.removeChild(self.cowProLabel);
        self.removeChild(self.chickProLabel);
        self.removeChild(self.labelMin);
        self.removeChild(self.labelSec);
    },
    addStoreBut: function(){
        this.storeBut = new StoreBut();
        this.storeBut.setPosition(new cc.Point( 710, 455 ));
        this.addChild(this.storeBut);
    },
    updateScore: function(dt){
        this.scoreLabel.setString(curentScore);
    },
    switchScene: function(index) {
        var self = this;
        if(self.direction == StartScene.DIR.FARM){            
            self.direction = StartScene.DIR.LEVEL;
            self.removeAllLabel();
            self.removeChild(self.currentScene);
            self.currentScene = self.levelScene;
            self.currentScene.init();
            self.addChild(self.currentScene);
            for(var i = 0;i<4;i++){
                chickFoodCheck[i] = false;
                cowFoodCheck[i] = false;
            }
            openStore = false;
                
        }else if(self.direction == StartScene.DIR.LEVEL){
            self.direction = StartScene.DIR.FARM; 
            self.removeChild(self.currentScene);
            self.removeChild(self.scoreBoard);
            self.removeChild(self.storeBut);
            self.currentScene = self.farmScene; 
            self.currentScene.init();
            self.addChild(self.currentScene);  
            self.setLevelData();
            self.setLevel(index);
            self.addScoreBoard();
            self.addMouseHandlers();
            self.addScoreLabel();
            self.addGoalLabel();
            self.addCurrentProLabel();
            self.addStoreBut();
            self.addClockBase();
            self.schedule(self.updateScore,0.01);
            self.schedule(self.countdown,1);
            self.scheduleOnce(function(){
            playing = true;
            },1);
            self.schedule(self.updateCurrentPro,0.01);
            

            openStore = false;
            for(var i = 0;i<4;i++){
                chickFoodCheck[i] = false;
                cowFoodCheck[i] = false;
            }
        }
    },
    switchToStart: function(){
            this.direction = StartScene.DIR.START; 
            this.removeChild(this.currentScene);
            this.removeChild(this.scoreBoard);
            this.removeChild(this.storeBut);
            this.currentScene = this.startPage; 
            this.currentScene.init();
            this.addChild(this.currentScene); 
    },
    switchToLevel: function(){
            this.direction = StartScene.DIR.LEVEL; 
            this.removeChild(this.currentScene);
            this.removeChild(this.scoreBoard);
            this.removeChild(this.storeBut);
            this.currentScene = this.levelScene; 
            this.currentScene.init();
            this.addChild(this.currentScene); 
    },
    addScoreBoard: function(){
        this.scoreBoard = new cc.Sprite.create("res/images/Board/scoreBoard.png");
        this.scoreBoard.setPosition(new cc.Point(650,505));
        this.addChild(this.scoreBoard);

        this.productGoal = new cc.Sprite.create("res/images/Board/productGoal.png");
        this.productGoal.setPosition(new cc.Point(355,505));
        this.addChild(this.productGoal);
    },
    openStore: function(){
        this.store = new Store();
        this.store.setPosition(new cc.Point(400,300));
        this.addChild(this.store);

    }
   
});
StartScene.DIR = {
    FARM: 1,
    LEVEL: 2,
    START: 3
};
