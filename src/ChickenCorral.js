var chickQueue = [];
var chickCheck = [];
var chickFoodCheck = [];
var chickFoodQueue = [];
var eggQueue = [];
var eggCheck = [];
var ChickenCorral = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/ChickenCorral/chickenCorral.png' );
        this.addMouseHandlers();
        this.chickList = [];
        this.chickListCheck = [];
        this.schedule(this.changeBox,0.01);

        this.chick1Loc = new cc.Point(141.00185528756958,120);
        this.chick2Loc = new cc.Point(298.33024118738405,120);
        this.chick3Loc = new cc.Point(141.00185528756958,50);
        this.chick4Loc = new cc.Point(298.33024118738405,50);
    
        for(var i = 0;i<4;i++){this.chickList.push(new Chicken(i));}
        for(var i = 0;i<4;i++){chickCheck.push(false);}
        for(var i = 0;i<4;i++){chickFoodCheck.push(false);}
        for(var i = 0;i<4;i++){eggCheck.push(null);}

        this.box1 = new cc.Sprite.create('res/images/ChickenCorral/empty.png');
        this.box1.setPosition(new cc.Point(109.77131,120));
        this.addChild(this.box1);

        this.box2 = new cc.Sprite.create('res/images/ChickenCorral/empty.png');
        this.box2.setPosition(new cc.Point(270.31527,120));
        this.addChild(this.box2);

        this.box3 = new cc.Sprite.create('res/images/ChickenCorral/empty.png');
        this.box3.setPosition(new cc.Point(109.77131,50));
        this.addChild(this.box3);

        this.box4 = new cc.Sprite.create('res/images/ChickenCorral/empty.png');
        this.box4.setPosition(new cc.Point(270.31527,50));
        this.addChild(this.box4);

        this.chickboxes1 = new cc.Sprite.create('res/images/ChickenCorral/empty.png');
        this.chickboxes2 = new cc.Sprite.create('res/images/ChickenCorral/empty.png');
        this.chickboxes3 = new cc.Sprite.create('res/images/ChickenCorral/empty.png');
        this.chickboxes4 = new cc.Sprite.create('res/images/ChickenCorral/empty.png');
        

        this.addMouseHandlers();
    },
    chooseEffect:function() {
    var audioengine = cc.audioEngine;
    audioengine.playEffect("res/sounds/choose.wav");
    },
    addChicken: function(loc){
        var self = this;
        console.log("loc : "+loc);
        if(loc == 1&&curentScore>=200&&!(self.chickList[0].bought)){
            if(chickNum<chickenGoal){
                chickNum++;
            }
            curentScore-=200;
            self.chickList[0].bought = true;
            chickCheck[0] = true;
            self.chickboxes1.setTexture('res/images/ChickenCorral/hasChick.png');
            self.chickboxes1.setPosition(new cc.Point(109.77131,120));
            self.chickList[0].setPosition(this.chick1Loc);
            self.addChild(self.chickboxes1);
            self.addChild(self.chickList[0]);

            self.chickList[0].runAction( self.chickList[0].movingAction1 );
            self.chickList[0].schedule(self.chickList[0].killChick,1);

        }else if(loc == 2&&curentScore>=200&&!(self.chickList[1].bought)){
            if(chickNum<chickenGoal){
                chickNum++;
            }
            curentScore-=200;
            self.chickList[1].bought = true;
            chickCheck[1] = true;
            self.chickboxes2.setTexture('res/images/ChickenCorral/hasChick.png');
            self.chickboxes2.setPosition(new cc.Point(270.31527,120));
            self.chickList[1].setPosition(this.chick2Loc);
            self.addChild(self.chickboxes2);
            self.addChild(self.chickList[1]);

            self.chickList[1].runAction( self.chickList[1].movingAction1 );
            self.chickList[1].schedule(self.chickList[1].killChick,1);
        }else if(loc == 3&&curentScore>=200&&!(self.chickList[2].bought)){
            if(chickNum<chickenGoal){
                chickNum++;
            }
            console.log("enter add Chick3!")
            curentScore-=200;   
            self.chickList[2].bought = true;
            chickCheck[2] = true;
            self.chickboxes3.setTexture('res/images/ChickenCorral/hasChick.png');
            self.chickboxes3.setPosition(new cc.Point(109.77131,50));
            self.chickList[2].setPosition(this.chick3Loc);
            self.addChild(self.chickboxes3);
            self.addChild(self.chickList[2]);

            self.chickList[2].runAction( self.chickList[2].movingAction1 );
            self.chickList[2].schedule(self.chickList[2].killChick,1);
        }
        else if(loc == 4&&curentScore>=200&&!(self.chickList[3].bought)){
            if(chickNum<chickenGoal){
                chickNum++;
            }
            curentScore-=200;
            self.chickList[3].bought = true;
            chickCheck[3] = true;
            self.chickboxes4.setTexture('res/images/ChickenCorral/hasChick.png');
            self.chickboxes4.setPosition(new cc.Point(270.31527,50));
            self.chickList[3].setPosition(this.chick4Loc);
            self.addChild(self.chickboxes4);
            self.addChild(self.chickList[3]);

            self.chickList[3].runAction( self.chickList[3].movingAction1 );
            self.chickList[3].schedule(self.chickList[3].killChick,1);
        }
        
    },
    changeBox: function(){
        self = this;
        if(!self.chickList[0].bought){
            self.chickboxes1.setTexture('res/images/ChickenCorral/empty.png');
            chickCheck[0] = false;
            self.removeChild(self.chickboxes1);
        }
        if(!self.chickList[1].bought){
            self.chickboxes2.setTexture('res/images/ChickenCorral/empty.png');
            chickCheck[1] = false;
            self.removeChild(self.chickboxes2);
        }
        if(!self.chickList[2].bought){
            self.chickboxes3.setTexture('res/images/ChickenCorral/empty.png');
            chickCheck[2] = false;
            self.removeChild(self.chickboxes3);
        }
        if(!self.chickList[3].bought){
            self.chickboxes4.setTexture('res/images/ChickenCorral/empty.png');
            chickCheck[3] = false;
            self.removeChild(self.chickboxes4);
        }
    },
    addChickenFood: function(loc){
        var self = this;
        console.log("loc : "+loc);
        console.log("index = "+chickFoodCheck[loc-1]);
        if(loc == 1&&curentScore>=20&&!chickFoodCheck[0]){
            console.log("add food1");
            curentScore-=20
            chickFoodCheck[0] = true;
            self.chickFood1 = new ChickFood(0,29.52806652806653,120,0);
            self.chickFood1.setPosition(new cc.Point(76.52806652806653,120));
            self.addChild(self.chickFood1);
        }else if(loc == 2&&curentScore>=20&&!chickFoodCheck[1]){
            console.log("add food2");
            curentScore-=20
            chickFoodCheck[1] = true;
            self.chickFood1 = new ChickFood(1,192.50103950103951,120,1);
            self.chickFood1.setPosition(new cc.Point(239.50103950103951,120));
            self.addChild(self.chickFood1);
        }else if(loc == 3&&curentScore>=20&&!chickFoodCheck[2]){
            console.log("add food3");
            curentScore-=20
            chickFoodCheck[2] = true;
            self.chickFood1 = new ChickFood(2,29.52806652806653,47,2);
            self.chickFood1.setPosition(new cc.Point(76.52806652806653,50));
            self.addChild(self.chickFood1);
        }
        else if(loc == 4&&curentScore>=20&&!chickFoodCheck[3]){
            console.log("add food4");
            curentScore-=20;
            chickFoodCheck[3] = true;
            self.chickFood1 = new ChickFood(3,192.50103950103951,47,3);
            self.chickFood1.setPosition(new cc.Point(239.50103950103951,50));
            self.addChild(self.chickFood1);
        }
    },
    addChickBack: function(){
          var self = this;
        if(chickCheck[0]){
            self.chickList[0].bought = true;
            self.chickboxes1.setPosition(new cc.Point(109.77131,120));
            self.chickList[0].setPosition(this.chick1Loc);
            self.addChild(self.chickboxes1);
            self.addChild(self.chickList[0]);

            self.chickList[0].runAction( self.chickList[0].movingAction1 );
            self.chickList[0].schedule(self.chickList[0].killChick,1);
        }
        if(chickCheck[1]){
            self.chickList[1].bought = true;
            self.chickboxes2.setPosition(new cc.Point(270.31527,120));
            self.chickList[1].setPosition(this.chick2Loc);
            self.addChild(self.chickboxes2);
            self.addChild(self.chickList[1]);

            self.chickList[1].runAction( self.chickList[1].movingAction1 );
            self.chickList[1].schedule(self.chickList[1].killChick,1);
        }
        if(chickCheck[2]){
            self.chickList[2].bought = true;
            self.chickboxes3.setPosition(new cc.Point(109.77131,50));
            self.chickList[2].setPosition(this.chick3Loc);
            self.addChild(self.chickboxes3);
            self.addChild(self.chickList[2]);

            self.chickList[2].runAction( self.chickList[2].movingAction1 );
            self.chickList[2].schedule(self.chickList[2].killChick,1);
        }
        if(chickCheck[3]){
            self.chickList[3].bought = true;
            self.chickboxes4.setPosition(new cc.Point(270.31527,50));
            self.chickList[3].setPosition(this.chick4Loc);
            self.addChild(self.chickboxes4);
            self.addChild(self.chickList[3]);

            self.chickList[3].runAction( self.chickList[3].movingAction1 );
            self.chickList[3].schedule(self.chickList[3].killChick,1);
        }
    },
    addMouseHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
        onMouseDown: function(event){
        	var loc = event.getLocation();
    
            if(startScene.direction == StartScene.DIR.FARM&&playing){
        	   if(loc.x >=48.23285&&loc.x<=171.30977&&loc.y<=232.84823&&loc.y>=171.30977){
                    if(!(self.chickList[0].bought)&&chickQueue[chickQueue.length-1]!=1){
                        chickQueue.push(1);
                    }else if((self.chickList[0].bought)&&chickFoodQueue[chickFoodQueue.length-1]!=1){
                        chickFoodQueue.push(1);
                    }
        	   }
                else if(loc.x >=207.90021&&loc.x<=332.64033&&loc.y<=232.84823&&loc.y>=171.30977){
                    if(!(self.chickList[1].bought)&&chickQueue[chickQueue.length-1]!=2){
                        chickQueue.push(2);
                    }else if((self.chickList[1].bought)&&chickFoodQueue[chickFoodQueue.length-1]!=2){
                        chickFoodQueue.push(2);
                    }
        	   }else if(loc.x >=48.23285&&loc.x<=171.30977&&loc.y<=159.66736&&loc.y>=99.79210){
                    if(!(self.chickList[2].bought)&&chickQueue[chickQueue.length-1]!=3){
                        chickQueue.push(3);
                    }else if((self.chickList[2].bought)&&chickFoodQueue[chickFoodQueue.length-1]!=3){
                        chickFoodQueue.push(3);
                    }
        	   }else if(loc.x >=207.90021&&loc.x<=332.64033&&loc.y<=159.66736&&loc.y>=99.79210){
				    if(!(self.chickList[3].bought)&&chickQueue[chickQueue.length-1]!=4){
                        chickQueue.push(4);
                    }else if((self.chickList[3].bought)&&chickFoodQueue[chickFoodQueue.length-1]!=4){
                        chickFoodQueue.push(4);
                    }
        	   }else if(eggCheck[0]!=null && loc.x>=29-15 && loc.x<=29+15 && loc.y>=201-15 && loc.y<=201+15&&eggQueue[eggQueue.length-1]!=1){
                    eggQueue.push(1);
               }else if(eggCheck[1]!=null && loc.x>=192-15 && loc.x<=192+15 && loc.y>=201-15 && loc.y<=201+15&&eggQueue[eggQueue.length-1]!=2){
                    eggQueue.push(2);
               }else if(eggCheck[2]!=null && loc.x>=29-15 && loc.x<=29+15 && loc.y>=127-15 && loc.y<=127+15&&eggQueue[eggQueue.length-1]!=3){
                    eggQueue.push(3);
               }else if(eggCheck[3]!=null && loc.x>=192-15 && loc.x<=192+15 && loc.y>=127-15 && loc.y<=127+15&&eggQueue[eggQueue.length-1]!=4){
                    eggQueue.push(4);
               }
            }
        },
        onMouseMove: function(event){
        	var loc = event.getLocation();
            if(startScene.direction == StartScene.DIR.FARM){
        	   if(loc.x >=48.23285&&loc.x<=171.30977&&loc.y<=232.84823&&loc.y>=171.30977){
                    if(!self.chickList[0].bought){
                        self.box1.setTexture('res/images/ChickenCorral/buyChick.png');
                    }else{
                        self.chickboxes1.setTexture('res/images/ChickenCorral/hasChickChoose.png');
                    }
        	   }else if(loc.x >=207.90021&&loc.x<=332.64033&&loc.y<=232.84823&&loc.y>=171.30977){
                    if(!self.chickList[1].bought){
                        self.box2.setTexture('res/images/ChickenCorral/buyChick.png');
                    }else{
                        self.chickboxes2.setTexture('res/images/ChickenCorral/hasChickChoose.png');
                    }
        		
        	   }else if(loc.x >=48.23285&&loc.x<=171.30977&&loc.y<=159.66736&&loc.y>=99.79210){
                    if(!self.chickList[2].bought){
                        self.box3.setTexture('res/images/ChickenCorral/buyChick.png');
                    }else{
                        self.chickboxes3.setTexture('res/images/ChickenCorral/hasChickChoose.png');
                    }

        	   }else if(loc.x >=207.90021&&loc.x<=332.64033&&loc.y<=159.66736&&loc.y>=99.79210){
				    if(!self.chickList[3].bought){
                        self.box4.setTexture('res/images/ChickenCorral/buyChick.png');
                    }else{
                        self.chickboxes4.setTexture('res/images/ChickenCorral/hasChickChoose.png');
                    }
        	   }else{
        		    self.box1.setTexture('res/images/ChickenCorral/empty.png');
        		    self.box2.setTexture('res/images/ChickenCorral/empty.png');
        		    self.box3.setTexture('res/images/ChickenCorral/empty.png');
        		    self.box4.setTexture('res/images/ChickenCorral/empty.png');
                    self.chickboxes1.setTexture('res/images/ChickenCorral/hasChick.png');
                    self.chickboxes2.setTexture('res/images/ChickenCorral/hasChick.png');
                    self.chickboxes3.setTexture('res/images/ChickenCorral/hasChick.png');
                    self.chickboxes4.setTexture('res/images/ChickenCorral/hasChick.png');
        	   }
            }

        }
    }, this);
    },
   

});
