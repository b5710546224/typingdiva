var eggSelling = false;
var milkSelling = false;
var Store = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/Store/Store.png' );
        this.addAllBut();
        this.addAllNumProduct();
        this.addAllNumSale();
        this.addMouseHandlers();
        this.schedule(this.updateProductNum,0.01);

        this.numSaleEgg = 0;
        this.numSaleMilk = 0;
    },
    addAllBut: function(){
        this.but1 = new SaleBut();
        this.but1.setPosition( new cc.Point( 485, 337) );
        this.addChild(this.but1);
        this.but2 = new SaleBut();
        this.but2.setPosition( new cc.Point( 485, 260) );
        this.addChild(this.but2);
    },
    addAllNumProduct: function(){
        this.eggNumber = cc.LabelTTF.create( '0', 'Arial', 30 );
        this.eggNumber.setPosition( new cc.Point( 250, 337 ) );
        this.addChild(this.eggNumber);
        this.milkNumber = cc.LabelTTF.create( '0', 'Arial', 30 );
        this.milkNumber.setPosition( new cc.Point( 250, 260 ) );
        this.addChild(this.milkNumber);
    },
    addAllNumSale: function(){
        this.eggNumSale = cc.LabelTTF.create( '0', 'Arial', 30 );
        this.eggNumSale.setPosition( new cc.Point( 359, 337 ) );
        this.addChild(this.eggNumSale);
        this.milkNumSale = cc.LabelTTF.create( '0', 'Arial', 30 );
        this.milkNumSale.setPosition( new cc.Point( 359, 260 ) );
        this.addChild(this.milkNumSale);
    },
    updateProductNum: function(dt){
        this.eggNumber.setString(eggNum);
        this.milkNumber.setString(milkNum);
    },
    saleEgg: function(price){
        curentScore+=price;
        eggSelling = false;
    },
    saleMilk: function(price){
        curentScore+=price;
        milkSelling = false;
    },
    addMouseHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
            onMouseDown: function(event){
                var loc = event.getLocation();
                if(loc.x>=517&&loc.x<=646&&loc.y>=392&&loc.y<=430&&!eggSelling){
                    if(self.numSaleEgg == 0){
                        self.but1.setTexture('res/images/Store/SaleBut3.png');
                    }else{
                        self.but1.setTexture('res/images/Store/selling.png');
                        var eggSell = self.numSaleEgg;
                        eggNum-=self.numSaleEgg;
                        self.numSaleEgg = 0;
                        eggSelling = true;
                        self.scheduleOnce(function(){
                        self.saleEgg(eggSell*100);
                        self.eggNumSale.setString(self.numSaleEgg);
                        }, 5);
                    }
                    
                }else if(loc.x>=517&&loc.x<=646&&loc.y>=317&&loc.y<=354&&!milkSelling){
                    if(self.numSaleMilk == 0){
                        self.but2.setTexture('res/images/Store/SaleBut3.png');
                    }else{
                        self.but2.setTexture('res/images/Store/selling.png');
                        var milkSell = self.numSaleMilk;
                        milkNum-=self.numSaleMilk;
                        self.numSaleMilk = 0;
                        milkSelling = true;
                        self.scheduleOnce(function(){
                        self.saleMilk(milkSell*500);
                        self.milkNumSale.setString(self.numSaleMilk);
                        }, 10);
                    }
                }else if(loc.x>=483&&loc.x<=502&&loc.y>=375&&loc.y<=394&&!eggSelling){
                    if(self.numSaleEgg+1<=eggNum){
                        self.numSaleEgg++;
                        self.eggNumSale.setString(self.numSaleEgg);
                    }else{
                        self.numSaleEgg=0;
                        self.eggNumSale.setString(self.numSaleEgg);
                    }
                }else if(loc.x>=483&&loc.x<=502&&loc.y>=301&&loc.y<=319&&!milkSelling){
                    if(self.numSaleMilk+1<=milkNum){
                        self.numSaleMilk++;
                        self.milkNumSale.setString(self.numSaleMilk);
                    }else{
                        self.numSaleMilk=0;
                        self.milkNumSale.setString(self.numSaleMilk);
                    }
                }
            },
            onMouseMove: function(event){
                var loc = event.getLocation();
                if(loc.x>=517&&loc.x<=646&&loc.y>=392&&loc.y<=430&&!eggSelling){
                    self.but1.setTexture('res/images/Store/SaleBut2.png');
                }else if(loc.x>=517&&loc.x<=646&&loc.y>=317&&loc.y<=354&&!milkSelling){
                    self.but2.setTexture('res/images/Store/SaleBut2.png');
                }else{
                    if(!eggSelling){self.but1.setTexture('res/images/Store/SaleBut1.png');}
                    else if(!milkSelling){self.but2.setTexture('res/images/Store/SaleBut1.png');}   
                }
            }
        }, this);
    }
});