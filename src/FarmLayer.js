var FarmLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
         
        this.setBgFarm();
        this.addChickenCor();
        this.addCowCor();
        this.addPlayer();
        this.playEffect();
        this.schedule(this.playEffect,47);

        return true;
    },
    playEffect:function() {
    var audioengine = cc.audioEngine;
    audioengine.playEffect("res/sounds/bgSound.mp3");
    },
    setBgFarm: function(){
        this.bgfarm = new cc.Sprite.create("res/images/farmBack.png");
        this.bgfarm.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
        this.addChild(this.bgfarm);
    },

    addChickenCor: function(){
        this.chickenCor = new ChickenCorral();
        this.chickenCor.setPosition(new cc.Point(190,250));
        this.addChild(this.chickenCor);
    },

    addCowCor: function(){
        this.cowCor = new CowCorral();
        this.cowCor.setPosition(new cc.Point(580,150));
        this.addChild(this.cowCor);
    },

    addPlayer: function(){
        this.player = new Player();
        this.player.setPosition(new cc.Point(720,100));
        this.addChild(this.player);
    }
});