var Chicken = cc.Sprite.extend({
    ctor: function(index) {
        this._super();
        this.initWithFile( 'res/images/Chicken/chick1.png' );
        this.movingAction1 = this.createAnimationAction1();
        this.movingAction2 = this.createAnimationAction2();
        this.bought = false;
        this.index = index;
        this.count = 0;
        this.almostdie = false;

    },
    createAnimationAction1: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/Chicken/chick1.png' );
        animation.addSpriteFrameWithFile( 'res/images/Chicken/chick2.png' );
        console.log( animation.getDelayPerUnit() );
        animation.setDelayPerUnit( 0.5 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    createAnimationAction2: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/Chicken/redChick1.png' );
        animation.addSpriteFrameWithFile( 'res/images/Chicken/redChick2.png' );
        console.log( animation.getDelayPerUnit() );
        animation.setDelayPerUnit( 0.5 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    killChick: function(){
        self = this;
        if(self.count >= 15 &&self.count < 20 &&!chickFoodCheck[this.index]&&!self.almostdie){
            self.stopAction(self.movingAction1);
            self.runAction(self.movingAction2);
            self.almostdie = true;
        }
        if(self.count >= 20 &&!chickFoodCheck[this.index]){
            self.bought = false;
            startScene.currentScene.chickenCor.removeChild(self);
            self.almostdie = false;
            self.count=0;
        }
        if(!chickFoodCheck[this.index]){
            self.count++;
        }else{
            if(self.almostdie){
                self.stopAction(self.movingAction2);
                self.runAction(self.movingAction1);
            }
            self.count=0;
            self.almostdie = false;
        }
    }
});

