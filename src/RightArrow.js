var RightArrow = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/Arrow/rightArrow1.png' );
        this.addMouseHandlers();
    },
    addMouseHandlers: function() {
    	var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
        onMouseMove: function(event){
            var getLoc = event.getLocation();
            if(getLoc.y<=328&&getLoc.y>=258&&getLoc.x>=21&&getLoc.x<=134){
            self.setTexture('res/images/Arrow/rightArrow2.png');    
            }else{
            self.setTexture( 'res/images/Arrow/rightArrow1.png' );
            }
        }
    }, this);
    }
});