var cowQueue = [];
var cowCheck = [];
var cowFoodCheck = [];
var cowFoodQueue = [];
var milkQueue = [];
var milkCheck = [];
var CowCorral = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/CowCorral/cowCorral.png' );
        this.addMouseHandlers();
        this.cowList = [];
        this.cowListCheck = [];
        this.schedule(this.changeBox,0.01);

        this.cow1Loc = new cc.Point(72,99);
        this.cow2Loc = new cc.Point(158,99);
        this.cow3Loc = new cc.Point(244,99);
        this.cow4Loc = new cc.Point(330,99);

        for(var i = 0;i<4;i++){
        	this.cowList.push(new Cow(i));
        	cowCheck.push(false);
        	cowFoodCheck.push(false);
        	milkCheck.push(null);
        }

        this.box1 = new cc.Sprite.create('res/images/CowCorral/buyCow2.png');
        this.box1.setPosition(new cc.Point(72,99));
        this.addChild(this.box1);

        this.box2 = new cc.Sprite.create('res/images/CowCorral/buyCow2.png');
        this.box2.setPosition(new cc.Point(158,99));
        this.addChild(this.box2);

		this.box3 = new cc.Sprite.create('res/images/CowCorral/buyCow2.png');
        this.box3.setPosition(new cc.Point(244,99));
        this.addChild(this.box3);

		this.box4 = new cc.Sprite.create('res/images/CowCorral/buyCow2.png');
        this.box4.setPosition(new cc.Point(330,99));
        this.addChild(this.box4);


        this.cowboxes1 = new cc.Sprite.create('res/images/CowCorral/cowEmpty.png');
        this.cowboxes2 = new cc.Sprite.create('res/images/CowCorral/cowEmpty.png');
        this.cowboxes3 = new cc.Sprite.create('res/images/CowCorral/cowEmpty.png');
        this.cowboxes4 = new cc.Sprite.create('res/images/CowCorral/cowEmpty.png');

        this.addMouseHandlers();

    },
    //-----------------------------------
       addCow: function(loc){
        var self = this;
        console.log("loc : "+loc);
        if(loc == 1&&curentScore>=1000&&!(self.cowList[0].bought)){
            curentScore-=1000;
            self.cowList[0].bought = true;
            cowCheck[0] = true;
            self.cowboxes1.setTexture('res/images/CowCorral/hasCow.png');
            self.cowboxes1.setPosition(new cc.Point(72,99));
            self.cowList[0].setPosition(this.cow1Loc);
            self.addChild(self.cowboxes1);
            self.addChild(self.cowList[0]);

            self.cowList[0].runAction( self.cowList[0].movingAction1 );
            self.cowList[0].schedule(self.cowList[0].killCow,1);

        }else if(loc == 2&&curentScore>=1000&&!(self.cowList[1].bought)){
            curentScore-=1000;
            self.cowList[1].bought = true;
            cowCheck[1] = true;
            self.cowboxes2.setTexture('res/images/CowCorral/hasCow.png');
            self.cowboxes2.setPosition(new cc.Point(158,99));
            self.cowList[1].setPosition(this.cow2Loc);
            self.addChild(self.cowboxes2);
            self.addChild(self.cowList[1]);

            self.cowList[1].runAction( self.cowList[1].movingAction1 );
            self.cowList[1].schedule(self.cowList[1].killCow,1);
        }else if(loc == 3&&curentScore>=1000&&!(self.cowList[2].bought)){
            curentScore-=1000;   
            self.cowList[2].bought = true;
            cowCheck[2] = true;
            self.cowboxes3.setTexture('res/images/CowCorral/hasCow.png');
            self.cowboxes3.setPosition(new cc.Point(244,99));
            self.cowList[2].setPosition(this.cow3Loc);
            self.addChild(self.cowboxes3);
            self.addChild(self.cowList[2]);

            self.cowList[2].runAction( self.cowList[2].movingAction1 );
            self.cowList[2].schedule(self.cowList[2].killCow,1);
        }
        else if(loc == 4&&curentScore>=1000&&!(self.cowList[3].bought)){
            curentScore-=1000;
            self.cowList[3].bought = true;
            cowCheck[3] = true;
            self.cowboxes4.setTexture('res/images/CowCorral/hasCow.png');
            self.cowboxes4.setPosition(new cc.Point(330,99));
            self.cowList[3].setPosition(this.cow4Loc);
            self.addChild(self.cowboxes4);
            self.addChild(self.cowList[3]);

            self.cowList[3].runAction( self.cowList[3].movingAction1 );
            self.cowList[3].schedule(self.cowList[3].killCow,1);
        }
        
    },
    changeBox: function(){
        self = this;
        if(!self.cowList[0].bought){
            self.cowboxes1.setTexture('res/images/CowCorral/cowEmpty.png');
            cowCheck[0] = false;
            self.removeChild(self.cowboxes1);
        }
        if(!self.cowList[1].bought){
            self.cowboxes2.setTexture('res/images/CowCorral/cowEmpty.png');
            cowCheck[1] = false;
            self.removeChild(self.cowboxes2);
        }
        if(!self.cowList[2].bought){
            self.cowboxes3.setTexture('res/images/CowCorral/cowEmpty.png');
            cowCheck[2] = false;
            self.removeChild(self.cowboxes3);
        }
        if(!self.cowList[3].bought){
            self.cowboxes4.setTexture('res/images/CowCorral/cowEmpty.png');
            cowCheck[3] = false;
            self.removeChild(self.cowboxes4);
        }
    },
    addCowFood: function(loc){
        var self = this;
        if(loc == 1&&curentScore>=20&&!cowFoodCheck[0]){
            curentScore-=20
            cowFoodCheck[0] = true;
            self.cowFood1 = new CowFood(0,449,92,0);
            self.cowFood1.setPosition(new cc.Point(67,20));
            self.addChild(self.cowFood1);
        }else if(loc == 2&&curentScore>=20&&!cowFoodCheck[1]){
            curentScore-=20
            cowFoodCheck[1] = true;
            self.cowFood1 = new CowFood(1,535,92,1);
            self.cowFood1.setPosition(new cc.Point(153,20));
            self.addChild(self.cowFood1);
        }else if(loc == 3&&curentScore>=20&&!cowFoodCheck[2]){
            curentScore-=20
            cowFoodCheck[2] = true;
            self.cowFood1 = new CowFood(2,622,92,2);
            self.cowFood1.setPosition(new cc.Point(239,20));
            self.addChild(self.cowFood1);
        }
        else if(loc == 4&&curentScore>=20&&!cowFoodCheck[3]){
            curentScore-=20;
            cowFoodCheck[3] = true;
            self.cowFood1 = new CowFood(3,708,92,3);
            self.cowFood1.setPosition(new cc.Point(325,20));
            self.addChild(self.cowFood1);
        }
    },
    addCowBack: function(){
          var self = this;
        if(cowCheck[0]){
            self.cowList[0].bought = true;
            self.cowboxes1.setPosition(new cc.Point(72,99));
            self.cowList[0].setPosition(this.cow1Loc);
            self.addChild(self.cowboxes1);
            self.addChild(self.cowList[0]);

            self.cowList[0].runAction( self.cowList[0].movingAction1 );
            self.cowList[0].schedule(self.cowList[0].killCow,1);
        }
        if(cowCheck[1]){
            self.cowList[1].bought = true;
            self.cowboxes2.setPosition(new cc.Point(158,99));
            self.cowList[1].setPosition(this.cow2Loc);
            self.addChild(self.cowboxes2);
            self.addChild(self.cowList[1]);

            self.cowList[1].runAction( self.cowList[1].movingAction1 );
            self.cowList[1].schedule(self.cowList[1].killCow,1);
        }
        if(cowCheck[2]){
            self.cowList[2].bought = true;
            self.cowboxes3.setPosition(new cc.Point(244,99));
            self.cowList[2].setPosition(this.cow3Loc);
            self.addChild(self.cowboxes3);
            self.addChild(self.cowList[2]);

            self.cowList[2].runAction( self.cowList[2].movingAction1 );
            self.cowList[2].schedule(self.cowList[2].killCow,1);
        }
        if(cowCheck[3]){
            self.cowList[3].bought = true;
            self.cowboxes4.setPosition(new cc.Point(330,99));
            self.cowList[3].setPosition(this.cow4Loc);
            self.addChild(self.cowboxes4);
            self.addChild(self.cowList[3]);

            self.cowList[3].runAction( self.cowList[3].movingAction1 );
            self.cowList[3].schedule(self.cowList[3].killCow,1);
        }
    },


    //--------------------------------------------
    addMouseHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
        onMouseDown: function(event){
        	var loc = event.getLocation();
             if(startScene.direction == StartScene.DIR.FARM){
               if(loc.x>=405&&loc.x<=493&&loc.y>=113&&loc.y<=204){
                    if(!(self.cowList[0].bought)&&cowQueue[cowQueue.length-1]!=1){
                        cowQueue.push(1);
                    }else if((self.cowList[0].bought)&&cowFoodQueue[cowFoodQueue.length-1]!=1){
                        cowFoodQueue.push(1);
                    }
               }
                else if(loc.x>=493&&loc.x<=578&&loc.y>=113&&loc.y<=204){
                    if(!(self.cowList[1].bought)&&cowQueue[cowQueue.length-1]!=2){
                        cowQueue.push(2);
                    }else if((self.cowList[1].bought)&&cowFoodQueue[cowFoodQueue.length-1]!=2){
                        cowFoodQueue.push(2);
                    }
               }else if(loc.x>=578&&loc.x<=666&&loc.y>=113&&loc.y<=204){
                    if(!(self.cowList[2].bought)&&cowQueue[cowQueue.length-1]!=3){
                        cowQueue.push(3);
                    }else if((self.cowList[2].bought)&&cowFoodQueue[cowFoodQueue.length-1]!=3){
                        cowFoodQueue.push(3);
                    }
               }else if(loc.x>=666&&loc.x<=750&&loc.y>=113&&loc.y<=204){
                    if(!(self.cowList[3].bought)&&cowQueue[cowQueue.length-1]!=4){
                        cowQueue.push(4);
                    }else if((self.cowList[3].bought)&&cowFoodQueue[cowFoodQueue.length-1]!=4){
                        cowFoodQueue.push(4);
                    }
               }else if(milkCheck[0]!=null && loc.x>=449-43 && loc.x<=449+43 && loc.y>=92-30 && loc.y<=92+30&&milkQueue[milkQueue.length-1]!=1){
                    milkQueue.push(1);
               }else if(milkCheck[1]!=null && loc.x>=535-43 && loc.x<=535+43 && loc.y>=92-30 && loc.y<=92+30&&milkQueue[milkQueue.length-1]!=2){
                    milkQueue.push(2);
               }else if(milkCheck[2]!=null && loc.x>=622-43 && loc.x<=622+43 && loc.y>=92-30 && loc.y<=92+30&&milkQueue[milkQueue.length-1]!=3){
                    milkQueue.push(3);
               }else if(milkCheck[3]!=null && loc.x>=708-43 && loc.x<=708+43 && loc.y>=92-30 && loc.y<=92+30&&milkQueue[milkQueue.length-1]!=4){
                    milkQueue.push(4);
               }
            }
          
        },
        onMouseMove: function(event){
        	var loc = event.getLocation();
          if(startScene.direction == StartScene.DIR.FARM){
               if(loc.x>=405&&loc.x<=493&&loc.y>=113&&loc.y<=204){
                    if(!self.cowList[0].bought){
                        self.box1.setTexture('res/images/CowCorral/buyCow.png');
                        self.box2.setTexture('res/images/CowCorral/buyCow2.png');
                        self.box3.setTexture('res/images/CowCorral/buyCow2.png');
                        self.box4.setTexture('res/images/CowCorral/buyCow2.png');
                    }else{
                        self.cowboxes1.setTexture('res/images/CowCorral/hasCowChoose.png');
                        self.cowboxes2.setTexture('res/images/CowCorral/hasCow.png');
                        self.cowboxes3.setTexture('res/images/CowCorral/hasCow.png');
                        self.cowboxes4.setTexture('res/images/CowCorral/hasCow.png');
                    }
               }else if(loc.x>=493&&loc.x<=578&&loc.y>=113&&loc.y<=204){
                    if(!self.cowList[1].bought){
                        self.box2.setTexture('res/images/CowCorral/buyCow.png');
                        self.box1.setTexture('res/images/CowCorral/buyCow2.png');
                        self.box3.setTexture('res/images/CowCorral/buyCow2.png');
                        self.box4.setTexture('res/images/CowCorral/buyCow2.png');
                    }else{
                        self.cowboxes2.setTexture('res/images/CowCorral/hasCowChoose.png');
                        self.cowboxes1.setTexture('res/images/CowCorral/hasCow.png');
                        self.cowboxes3.setTexture('res/images/CowCorral/hasCow.png');
                        self.cowboxes4.setTexture('res/images/CowCorral/hasCow.png');
                    }
                
               }else if(loc.x>=578&&loc.x<=666&&loc.y>=113&&loc.y<=204){
                    if(!self.cowList[2].bought){
                        self.box3.setTexture('res/images/CowCorral/buyCow.png');
                        self.box2.setTexture('res/images/CowCorral/buyCow2.png');
                        self.box1.setTexture('res/images/CowCorral/buyCow2.png');
                        self.box4.setTexture('res/images/CowCorral/buyCow2.png');
                    }else{
                        self.cowboxes3.setTexture('res/images/CowCorral/hasCowChoose.png');
                        self.cowboxes2.setTexture('res/images/CowCorral/hasCow.png');
                        self.cowboxes1.setTexture('res/images/CowCorral/hasCow.png');
                        self.cowboxes4.setTexture('res/images/CowCorral/hasCow.png');
                    }

               }else if(loc.x>=666&&loc.x<=750&&loc.y>=113&&loc.y<=204){
                    if(!self.cowList[3].bought){
                        self.box4.setTexture('res/images/CowCorral/buyCow.png');
                        self.box2.setTexture('res/images/CowCorral/buyCow2.png');
                        self.box3.setTexture('res/images/CowCorral/buyCow2.png');
                        self.box1.setTexture('res/images/CowCorral/buyCow2.png');
                    }else{
                        self.cowboxes4.setTexture('res/images/CowCorral/hasCowChoose.png');
                        self.cowboxes2.setTexture('res/images/CowCorral/hasCow.png');
                        self.cowboxes3.setTexture('res/images/CowCorral/hasCow.png');
                        self.cowboxes1.setTexture('res/images/CowCorral/hasCow.png');
                    }
               }else{
                    self.box1.setTexture('res/images/CowCorral/hasCow.png');
                    self.box2.setTexture('res/images/CowCorral/hasCow.png');
                    self.box3.setTexture('res/images/CowCorral/hasCow.png');
                    self.box4.setTexture('res/images/CowCorral/hasCow.png');
                    self.cowboxes1.setTexture('res/images/CowCorral/hasCow.png');
                    self.cowboxes2.setTexture('res/images/CowCorral/hasCow.png');
                    self.cowboxes3.setTexture('res/images/CowCorral/hasCow.png');
                    self.cowboxes4.setTexture('res/images/CowCorral/hasCow.png');
               }
            }


        }
    }, this);
    }
});