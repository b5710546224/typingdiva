var Milk = cc.Sprite.extend({
    ctor: function(x,y,n) {
        this._super();
        this.number = n;
        this.initWithFile( 'res/images/Milk/milk1.png' );
        this.movingAction = this.createAnimationAction();
        this.runAction( this.movingAction );
        if(startScene.direction == StartScene.DIR.FARM){
        	this.addMilk(x,y);
        }
        this.autoRemoveMilk();
        this.pos = 0;
    },
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/Milk/milk1.png' );
        animation.addSpriteFrameWithFile( 'res/images/Milk/milk2.png' );
        console.log( animation.getDelayPerUnit() );
        animation.setDelayPerUnit( 0.7 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    autoRemoveMilk: function(){
        this.scheduleOnce(function(){
        startScene.removeChild(this)
        },10);
        this.scheduleOnce(function(){
        milkCheck[this.number] = null;
        },10);
        
    },
    addMilk: function(x,y){
    	this.setPosition( new cc.Point(x,y) );
    	startScene.addChild(this);
        this.pos = this.getPosition();
        milkCheck[this.number] = this;
    },
});
