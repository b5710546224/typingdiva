var queue = [];
var Player = cc.Sprite.extend({

    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/Player/playerRight.png' );
        this.addMouseHandlers();
        this.position = this.getPosition();
        this.direction = Player.DIR.RIGHT;
        this.mousePosition = this.getPosition();
        this.arrowClicked = false;
        this.mousePos = 0;
    },
    update: function() {
        this.mousePos = queue[0];
    	var pos = this.getPosition();
        var speed = 4 ;
        if(pos.x<=this.mousePos.x+5 && pos.x>=this.mousePos.x-5){
            queue.shift();
            startScene.removeChild(tickBox.shift());
            if(chickQueue[0]!=0&&chickQueue.length!=0){
                    startScene.currentScene.chickenCor.addChicken(chickQueue[0]);
                    chickQueue.shift()
            }else{
                chickQueue.shift()
            }

             if(cowQueue[0]!=0&&cowQueue.length!=0){
                    startScene.currentScene.cowCor.addCow(cowQueue[0]);
                    cowQueue.shift()
            }else{
                cowQueue.shift()
            }
//------------------------------------
             if(chickFoodQueue[0]!=0&&chickFoodQueue.length!=0){
                    startScene.currentScene.chickenCor.addChickenFood(chickFoodQueue[0]);
                    chickFoodQueue.shift()
            }else{
                chickFoodQueue.shift()
            }
            if(cowFoodQueue[0]!=0&&cowFoodQueue.length!=0){
                    console.log("enter cow cowFood");
                    startScene.currentScene.cowCor.addCowFood(cowFoodQueue[0]);
                    cowFoodQueue.shift()
            }else{
                cowFoodQueue.shift()
            }

//------------------------------------
            if(eggQueue[0]!=0&&eggQueue.length!=0){
                    startScene.currentScene.chickenCor.removeChild(eggCheck[eggQueue[0]-1]);
                    eggCheck[eggQueue[0]-1]=null
                    eggQueue.shift()
                    eggNum++;
                    if(currentEggNum<eggGoal){
                        console.log("pluss currentEggNum");
                        currentEggNum++;
                    }
                    console.log("egg = "+eggNum);

            }else{
                eggQueue.shift()
            }
            if(milkQueue[0]!=0&&milkQueue.length!=0){

                    startScene.removeChild(milkCheck[milkQueue[0]-1]);
                    milkCheck[milkQueue[0]-1]=null
                    milkQueue.shift()
                    milkNum++;
                    if(currentMilkNum<milkGoal){
                        console.log("pluss currentMilkNum");
                        currentMilkNum++;
                    }
                    console.log("num milke "+milkNum);
            }else{
                milkQueue.shift()
            }

            if(queue.length!=0){
                this.scheduleUpdate();
            }else{
                this.unscheduleUpdate();
            }
        }
//------------------------------------
		if ( pos.x < this.mousePos.x ) {
			if(this.direction == Player.DIR.LEFT){
				this.switchDirection();
			}
	 	   		this.setPosition(new cc.Point(pos.x+speed,pos.y));
		} else {
			if(this.direction == Player.DIR.RIGHT){
				this.switchDirection();
			}
	    		this.setPosition(new cc.Point(pos.x-speed,pos.y));
		}
   	},  
   	switchDirection: function() {
		if ( this.direction == Player.DIR.LEFT ) {
	 	   	this.direction = Player.DIR.RIGHT;
	    	this.setTexture('res/images/Player/playerRight.png');
		} else {
	    	this.direction = Player.DIR.LEFT;
	    	this.setTexture('res/images/Player/playerLeft.png');
		}
    },
    addMouseHandlers: function() {
    	var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
        onMouseDown: function(event){
            var loc = event.getLocation();
            console.log(openStore+" "+queue.length+" "+playing);
            if(event.getLocation().y<=334.3035343035343&&!openStore&&playing){
                if(queue.length==0){
                    queue.push(event.getLocation());
                    if(startScene.direction == StartScene.DIR.FARM){
                        console.log(eggCheck);
                        if(loc.x >=48.23285&&loc.x<=171.30977&&loc.y<=232.84823&&loc.y>=171.30977){}
                        else if(loc.x >=207.90021&&loc.x<=332.64033&&loc.y<=232.84823&&loc.y>=171.30977){}
                        else if(loc.x >=48.23285&&loc.x<=171.30977&&loc.y<=159.66736&&loc.y>=99.79210){}
                        else if(loc.x >=207.90021&&loc.x<=332.64033&&loc.y<=159.66736&&loc.y>=99.79210){}
                        else if(eggCheck[0]!=null && loc.x>=29-15 && loc.x<=29+15 && loc.y>=201-15 && loc.y<=201+15){}
                        else if(eggCheck[1]!=null && loc.x>=192-15 && loc.x<=192+15 && loc.y>=201-15 && loc.y<=201+15){}
                        else if(eggCheck[2]!=null && loc.x>=29-15 && loc.x<=29+15 && loc.y>=127-15 && loc.y<=127+15){}
                        else if(eggCheck[3]!=null && loc.x>=192-15 && loc.x<=192+15 && loc.y>=127-15 && loc.y<=127+15){}
                        else{
                            console.log("get queue");
                            chickQueue.push(0);
                            chickFoodQueue.push(0);
                            eggQueue.push(0);
                        }
                    }
                    self.scheduleUpdate();
                }else{
                    queue.push(event.getLocation());
                    if(startScene.direction == StartScene.DIR.FARM){
                        if(loc.x >=48.23285&&loc.x<=171.30977&&loc.y<=232.84823&&loc.y>=171.30977){}
                        else if(loc.x >=207.90021&&loc.x<=332.64033&&loc.y<=232.84823&&loc.y>=171.30977){}
                        else if(loc.x >=48.23285&&loc.x<=171.30977&&loc.y<=159.66736&&loc.y>=99.79210){}
                        else if(loc.x >=207.90021&&loc.x<=332.64033&&loc.y<=159.66736&&loc.y>=99.79210){}
                        else if(eggCheck[0]!=null && loc.x>=29-15 && loc.x<=29+15 && loc.y>=201-15 && loc.y<=201+15){}
                        else if(eggCheck[1]!=null && loc.x>=192-15 && loc.x<=192+15 && loc.y>=201-15 && loc.y<=201+15){}
                        else if(eggCheck[2]!=null && loc.x>=29-15 && loc.x<=29+15 && loc.y>=127-15 && loc.y<=127+15){}
                        else if(eggCheck[3]!=null && loc.x>=192-15 && loc.x<=192+15 && loc.y>=127-15 && loc.y<=127+15){}
                        else if(loc.x>=405&&loc.x<=493&&loc.y>=113&&loc.y<=204){}
                        else if(loc.x>=493&&loc.x<=578&&loc.y>=113&&loc.y<=204){}
                        else if(loc.x>=578&&loc.x<=666&&loc.y>=113&&loc.y<=204){}
                        else if(loc.x>=666&&loc.x<=750&&loc.y>=113&&loc.y<=204){}
                        else if(milkCheck[0]!=null && loc.x>=449-43 && loc.x<=449+43 && loc.y>=92-30 && loc.y<=92+30){}
                        else if(milkCheck[1]!=null && loc.x>=535-43 && loc.x<=535+43 && loc.y>=92-30 && loc.y<=92+30){}
                        else if(milkCheck[2]!=null && loc.x>=622-43 && loc.x<=622+43 && loc.y>=92-30 && loc.y<=92+30){}
                        else if(milkCheck[3]!=null && loc.x>=708-43 && loc.x<=708+43 && loc.y>=92-30 && loc.y<=92+30){}    
                        else{
                            chickQueue.push(0);
                            chickFoodQueue.push(0);
                            eggQueue.push(0);
                            cowQueue.push(0);
                            cowFoodQueue.push(0);
                            milkQueue.push(0);
                        }
                    }
                }
            }
        }
    }, this);
    }
});
Player.DIR = {
    LEFT: 1,
    RIGHT: 2
};