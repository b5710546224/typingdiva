var ChickFood = cc.Sprite.extend({
    ctor: function(index,x,y,n) {
        this._super();
        this.number = n;
        this.initWithFile( 'res/images/ChickFood/chickFood1.png' );
        this.killItSelf(index,x,y);
    },
    killItSelf: function(index,x,y){

    	this.scheduleOnce(function(){
        this.setTexture('res/images/ChickFood/chickFood2.png');
        }, 1);
        this.scheduleOnce(function(){
        this.setTexture('res/images/ChickFood/chickFood3.png');
        }, 2);
        this.scheduleOnce(function(){
        this.setTexture('res/images/ChickFood/chickFood4.png');
        }, 3);
        this.scheduleOnce(function(){
        chickFoodCheck[index] = false
        }, 4);
        this.scheduleOnce(function(){
        startScene.currentScene.chickenCor.newEgg = new Egg(x,y,this.number);
        }, 4);
        this.scheduleOnce(function(){
        startScene.currentScene.chickenCor.removeChild(this)
        }, 4);

    }


});
