var FactoryLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
         
        this.setBgFactory();
        this.addRightArrow();
        this.addEggMachine();
        this.addPlayer();

        return true;
    },

    setBgFactory: function(){
        this.bgFactory = new cc.Sprite.create("res/images/factoryBG.png");
        this.bgFactory.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
        this.addChild(this.bgFactory);
    },
    addPlayer: function(){
        this.player = new Player();
        this.player.setPosition(new cc.Point(80,100));
        this.addChild(this.player);
    },

    addRightArrow: function(){
        this.rightArrow = new RightArrow();
        this.rightArrow.setPosition(new cc.Point(80,300));
        this.addChild(this.rightArrow);  
    },
    addEggMachine: function(){
        this.eggMachine = new EggMachine();
        this.eggMachine.setPosition(new cc.Point(400,240));
        this.addChild(this.eggMachine);
    }
});