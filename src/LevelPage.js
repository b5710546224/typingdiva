var markBoxCheck = [];
var LevelPage = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.markBox = [];
        this.markLoc = [];
        this.setBG();
        for(var i = 0;i<7;i++){this.markBox.push(new LevelBut());}
        for(var i = 0;i<7;i++){markBoxCheck.push(false);}

        this.markLoc.push(new cc.Point( 173,203 ));
        this.markLoc.push(new cc.Point( 357,160 ));
        this.markLoc.push(new cc.Point( 543,180 ));
        this.markLoc.push(new cc.Point( 660,356 ));
        this.markLoc.push(new cc.Point( 516,443 ));
        this.markLoc.push(new cc.Point( 129,478 ));
        this.markLoc.push(new cc.Point( 274,375 ));   
        this.addBackButton();
        this.setPos();
        this.addMark();
        this.addMouseHandlers();

        return true;
    },
    setBG: function(){
    	this.bgstart = new cc.Sprite.create("res/images/LevelPage/levelBG.png");
        this.bgstart.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
        this.addChild(this.bgstart);
    },
    setPos: function(){
        for(var i = 0;i<7;i++){this.markBox[i].setPosition( this.markLoc[i]);}   
    },
    addMark:function(){
        for(var i = 0;i<7;i++){this.addChild(this.markBox[i]);}
    },
    addBackButton: function(){
        this.backBut = new cc.Sprite.create("res/images/LevelPage/backBut1.png");
        this.backBut.setPosition(new cc.Point(705,50));
        this.addChild(this.backBut);
    },
    addMouseHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
        onMouseDown: function(event){
            var loc = event.getLocation();
            console.log(loc.x+" "+loc.y);
            for(var i = 0;i<7;i++){
                if(loc.x>=(self.markLoc[i].x-25)&&loc.x<=(self.markLoc[i].x+25)&&loc.y>=(self.markLoc[i].y-25)&&loc.y<=(self.markLoc[i].y+25)){
                    if(markBoxCheck[i]){
                        self.markBox[i].setTexture('res/images/LevelPage/blue3.png');
                        startScene.switchScene(i);
                    }else{
                        self.markBox[i].setTexture('res/images/LevelPage/red3.png');
                        if(i==0){
                            startScene.switchScene(i);
                        }else if(markBoxCheck[i-1]){
                            startScene.switchScene(i);
                        }
                        
                    }
                }else{
                     if(markBoxCheck[i]){
                        self.markBox[i].setTexture('res/images/LevelPage/blue1.png');
                    }else{
                        self.markBox[i].setTexture('res/images/LevelPage/red1.png');
                    }
                }
            }
            if(loc.x>=637&&loc.x<=768&&loc.y>=29&&loc.y<=71){
                self.backBut.setTexture('res/images/LevelPage/backBut3.png');
                startScene.switchToStart();
            }else{
                self.backBut.setTexture('res/images/LevelPage/backBut1.png');
            }
        },
        onMouseMove: function(event){
            var loc = event.getLocation();  
            for(var i = 0;i<7;i++){
                if(loc.x>=(self.markLoc[i].x-25)&&loc.x<=(self.markLoc[i].x+25)&&loc.y>=(self.markLoc[i].y-25)&&loc.y<=(self.markLoc[i].y+25)){
                    if(markBoxCheck[i]){
                        self.markBox[i].setTexture('res/images/LevelPage/blue2.png');
                    }else{
                        self.markBox[i].setTexture('res/images/LevelPage/red2.png');
                    }
                }else{
                     if(markBoxCheck[i]){
                        self.markBox[i].setTexture('res/images/LevelPage/blue1.png');
                    }else{
                        self.markBox[i].setTexture('res/images/LevelPage/red1.png');
                    }
                }
            }
            if(loc.x>=637&&loc.x<=768&&loc.y>=29&&loc.y<=71){
                self.backBut.setTexture('res/images/LevelPage/backBut2.png');
            }else{
                self.backBut.setTexture('res/images/LevelPage/backBut1.png');
            }

        }
    }, this);
    }
});