var CowFood = cc.Sprite.extend({
    ctor: function(index,x,y,n) {
        this._super();
        this.number = n;
        this.initWithFile( 'res/images/CowFood/cowFood1.png' );
        this.killItSelf(index,x,y);
    },
    killItSelf: function(index,x,y){

    	this.scheduleOnce(function(){
        this.setTexture('res/images/CowFood/cowFood2.png');
        }, 1);
        this.scheduleOnce(function(){
        this.setTexture('res/images/CowFood/cowFood3.png');
        }, 2);
        this.scheduleOnce(function(){
        this.setTexture('res/images/CowFood/cowFood4.png');
        }, 3);
        this.scheduleOnce(function(){
        this.setTexture('res/images/CowFood/cowFood5.png');
        }, 4);
        this.scheduleOnce(function(){
        this.setTexture('res/images/CowFood/cowFood6.png');
        }, 5);
        this.scheduleOnce(function(){
        this.setTexture('res/images/CowFood/cowFood7.png');
        }, 6);
        this.scheduleOnce(function(){
        this.setTexture('res/images/CowFood/cowFood8.png');
        }, 7);
        this.scheduleOnce(function(){
        this.setTexture('res/images/CowFood/cowFood9.png');
        }, 8);

        this.scheduleOnce(function(){
        cowFoodCheck[index] = false
        }, 9);
        this.scheduleOnce(function(){
        startScene.currentScene.cowCor.newMilk = new Milk(x,y,this.number);
        }, 9);
        this.scheduleOnce(function(){
        startScene.currentScene.cowCor.removeChild(this)
        }, 9);

    }


});