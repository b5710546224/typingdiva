var StartLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.setBG();
        this.addStartBut();
        this.addMouseHandlers();

        return true;
    },
    setBG: function(){
    	this.bgstart = new cc.Sprite.create("res/images/StartPage/startBG.png");
        this.bgstart.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
        this.addChild(this.bgstart);
    },
    addStartBut: function(){
    	this.startBut = new cc.Sprite.create("res/images/StartPage/startBut1.png");
    	this.startBut.setPosition( new cc.Point( 520,150 ) );
        this.addChild(this.startBut);
    },
    addMouseHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
        onMouseDown: function(event){
            var loc = event.getLocation();
            console.log(loc.x+" "+loc.y);
            if(loc.x>=316&&loc.x<=723&&loc.y>=119&&loc.y<=178){
                self.startBut.setTexture('res/images/StartPage/startBut3.png');
                startScene.switchToLevel();
            }else{
                self.startBut.setTexture('res/images/StartPage/startBut1.png');
            }
            
        },
        onMouseMove: function(event){
            var loc = event.getLocation();  
            if(loc.x>=316&&loc.x<=723&&loc.y>=119&&loc.y<=178){
                self.startBut.setTexture('res/images/StartPage/startBut2.png');
            }else{
                self.startBut.setTexture('res/images/StartPage/startBut1.png');
            }
          

        }
    }, this);
    }
});